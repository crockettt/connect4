import React from 'react';
import Cell from './Cell';

// Row component
const Row = ({ row, takeTurn }) => {
    return (
      <tr>
        {row.map((cell, i) => <Cell key={i} value={cell} columnIndex={i} takeTurn={takeTurn} />)}
      </tr>
    );
  };
  
export default Row;