import React, {Component} from 'react';
import './Modal.css';
import Backdrop from './Backdrop.js';

class Modal extends Component {
   render() {
      return (
               <>
       <Backdrop show={this.props.show} clicked={this.props.closed} />
                   <div className="Modal" style={{
                           transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                           opacity: this.props.show ? '.85' : '0'
                       }}>
                       {this.props.children}
                   </div>
              </> 
            )
   }
}

export default Modal;