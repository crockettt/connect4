import React from 'react';

const Cell = ({ value, columnIndex, takeTurn }) => {
    let color = value === null ? 'White' : value;
      
    return (
      <td>
        <div className="cell" onClick={() => {takeTurn(columnIndex)}}>
          <div className={color}></div>
        </div>
      </td>
    );
  };  
export default Cell;