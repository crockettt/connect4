import React from 'react';
import Row from './components/Row';
import Modal from './components/Modal';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      player1: 'Red',
      player2: 'Yellow',
      currentPlayer: null,
      board: [],
      gameEnded: false,
      showRestart: false,
      quitGame: false,
      message: ''
    };
    
    // Bind insertChecker function to App component
    this.insertChecker = this.insertChecker.bind(this);
  }
  
  componentWillMount() {
    // make sure to create the board when the component is ready.
    this.initBoard();
  }
  
  // Starts new game
  initBoard(player) {
    // Create a blank 7X6 matrix for the game
    let board = [];
    // board is seven rows
    for (let r = 0; r < 7; r++) {
      let row = [];
      // By six columns
      for (let c = 0; c < 6; c++) { row.push(null) }
      board.push(row);
    }
    let playerToStart = this.state.player1;

    if(player){
      playerToStart = player;
    }

    // Initialize all the state for the beginning of the game.
    this.setState({
      board,
      currentPlayer: playerToStart,
      gameEnded: false,
      showRestart: false,
      quitGame: false,
      message: playerToStart + ', please enter the column to drop your checker:'
    });
  }
  
  // Use this to switch players after each turn is taken
  switchPlayer() {
    return this.state.currentPlayer === this.state.player1 ? this.state.player2 : this.state.player1;
  }
  
  insertChecker(c) {
    // make sure the game has not ended
    if (this.state.gameEnded) {
      if(!this.state.showRestart){
        this.toggleModal();
      }
      return;
    }     
    // Place piece into board
    let board = this.state.board;
    let checkerInserted = false;
    for (let r = 6; r >= 0; r--) {
      if (!board[r][c]) {
        // Set the value of the matrix location to the player 
        board[r][c] = this.state.currentPlayer;
        checkerInserted = true;
        break;
      }
    }
    if(!checkerInserted){
      this.setState({ message: 'No spaces available in column. ' + this.state.currentPlayer + ', please enter a different column to drop your checker:' });
      return;
    }

    // Check status of board
    let result = this.checkAll(board); //Use function to see if a player has won
    if (!result) {
      // Update the state of the board and switch the player
      let altPlayer = this.switchPlayer();
      this.setState({ board, currentPlayer: altPlayer, message: altPlayer + ', please enter the column to drop your checker:' });
      return;
    }
    this.toggleModal();
    this.endGame(board, result);
  }
  
  checkVertical(board) {
    // Check only if row is 3 or greater
    // because it has to be to that row before you can have 4 in a row to begin with
    for (let r = 3; r < 7; r++) {
      for (let c = 0; c < 6; c++) {
        if (board[r][c]) {
          if (board[r][c] === board[r - 1][c] &&
              board[r][c] === board[r - 2][c] &&
              board[r][c] === board[r - 3][c]) {
            return board[r][c];    
          }
        }
      }
    }
  }
  
  checkHorizontal(board) {
    // Check only if column is 3 or less
    for (let r = 0; r < 7; r++) {
      for (let c = 0; c < 3; c++) {
        if (board[r][c]) {
          if (board[r][c] === board[r][c + 1] && 
              board[r][c] === board[r][c + 2] &&
              board[r][c] === board[r][c + 3]) {
            return board[r][c];
          }
        }
      }
    }
  }
  
  checkDiagonalRight(board) {
    // Check only if row is 3 or greater AND column is 3 or less
    for (let r = 3; r < 7; r++) {
      for (let c = 0; c < 3; c++) {
        if (board[r][c]) {
          if (board[r][c] === board[r - 1][c + 1] &&
              board[r][c] === board[r - 2][c + 2] &&
              board[r][c] === board[r - 3][c + 3]) {
            return board[r][c];
          }
        }
      }
    }
  }
  
  checkDiagonalLeft(board) {
    // Check only if row is 3 or greater AND column is 3 or greater
    for (let r = 3; r < 7; r++) {
      for (let c = 3; c < 6; c++) {
        if (board[r][c]) {
          if (board[r][c] === board[r - 1][c - 1] &&
              board[r][c] === board[r - 2][c - 2] &&
              board[r][c] === board[r - 3][c - 3]) {
            return board[r][c];
          }
        }
      }
    }
  }
  
  checkDraw(board) {
    // Loops all cells to see if they are all full.
    for (let r = 0; r < 7; r++) {
      for (let c = 0; c < 6; c++) {
        if (board[r][c] === null) {
          return null;
        }
      }
    }
    return 'draw';    
  }
  
  // Checks all possible directions to win and if the game ends in a draw.
  checkAll(board) {
    return this.checkVertical(board) || this.checkDiagonalRight(board) || this.checkDiagonalLeft(board) || this.checkHorizontal(board) || this.checkDraw(board);
  }
  
  endGame(board, result) {
    this.setState({ board, gameEnded: true, message: result === 'draw' ? 'Draw game.' : result + ' player wins!' });
  }

  // Use this to show the Modal
  toggleModal = () => {
    this.setState({ showRestart: !this.state.showRestart })
  }
  
  anotherRound = () => {
    this.initBoard(this.switchPlayer());
  }
  
  quitGame = () => {
    this.toggleModal(); 
    this.setState({ quitGame: true, message: "Thanks for playing"});
  }

  render() {
      return (
        <div className="App">
          <h2>Connect-4 sample app</h2>
          <br/>
          <table>
            <tbody>
              { this.state.quitGame ? <></> : this.state.board.map((row, i) => (<Row key={i} row={row} takeTurn={this.insertChecker} />))}
            </tbody>
          </table>
          <br/>
          <p className="message">{this.state.message}</p>
          <Modal show={this.state.showRestart} closed={this.toggleModal}>
              <h2>Game has ended</h2>
              <h3>Want to play again?</h3>
              <div className='btn-container'>
                  <button className="btn" onClick={this.quitGame}>No</button>
                  <button className="btn" onClick={this.anotherRound}>Yes</button>
              </div>
          </Modal>
        </div>
      );
    }
}

export default App;